<h1>Alphalogic Service Manager API
<a href="https://asm_api.readthedocs.io">
<img src="https://readthedocs.org/projects/asm_api/badge/?version=latest" alt="Documentation Status" />
</a>
</h1>
The official API for Alphalogic Service Manager. <a href="https://asm_api.readthedocs.io">   Documentation</a>
